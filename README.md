Name: Taeho Yun


Email: jawookr1995@gmail.com
# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.
 

## ACP controle

Acp controle time algorithm has minimum and maximum speed for each control location , which are 200km, 400km, 600km and 1000km. For each control location,
it has corresponding speed. (ie. 0-200 (min:15km/h, max: 34km/h) Therefore, for each control location and corresponding speed will let us to calculate time that
is taken to finish the race. Add each piece of time that came out from calculation and add them up for getting total time for race. Opening time will use
maximum speed and closing time will use minimum speed for calculation, rest of control locations will be calculated based on corresponding speeds.
Opening time Calculation would be, if total distance is 500 km and control points are at 200,400, with brevet at 600
so first 200km/34 = 5H53, next 200km/32 = 6H15, and last 100km/30 = 3H20, then 5H53+6H15+3H20 = 16H28
Same procedures for closing time, but just change denominotor to minimum speed.
The calculation for the residue from above calculation would be (residue/speed) * 60 (minutes)

