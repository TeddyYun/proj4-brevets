"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

# control location, followed by maximum speed in open, minimum speed in close
# 1300 = 13.333 for min 26 for max
OpenACP = [(1000,28),(600,30),(400,32),(200,34),(0,34)]
CloseACP = [(1000,11.428),(600,15),(400,15),(200,15),(0,15)]

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    i = 0
    total = 0

    for i in range(len(OpenACP)):
        if control_dist_km >= OpenACP[i][0]: # first element of location i
            km = control_dist_km - OpenACP[i][0]
            time = km / OpenACP[i-1][1] # distance / speed  = time
            total = total + time # add up the total time
            control_dist_km = control_dist_km - km # update total distance
    # for hours + minutes format
    brevet_start_time = arrow.get(brevet_start_time)
    hour = int(total)
    minute = round(60*(total - hour))
    opening = brevet_start_time.shift(hours = hour, minutes = minute)

    return opening.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    i = 0
    total = 0

    for i in range(len(CloseACP)):
        if control_dist_km >= CloseACP[i][0]:
            km = control_dist_km - CloseACP[i][0]
            time = km / CloseACP[i-1][1]
            total = total + time
            control_dist_km = control_dist_km - km
    brevet_start_time = arrow.get(brevet_start_time)
    hour = int(total)
    minute = round(60*(total - hour))
    closing = brevet_start_time.shift(hours = hour, minutes = minute)
    return closing.isoformat()
