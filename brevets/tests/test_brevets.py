# This is nose test for testing acp_times.py

import acp_times
import nose
import logging
import arrow

logging.basicConfig(format = '%(levelname)s:%(message)s', level = logging.WARNING)
log = logging.getLogger(__name__)

def small_value():
    '''
    This test is for testing distance which is smaller than 200km.
    '''

    date = arrow.Arrow(2020,11,9)

    assert acp_times.open_time(150,200,arrow.get(date))==(date.shift(hours=4, minutes =25)).isoformat()
    assert acp_times.close_time(150,200,arrow.get(date)) == (date.shift(hours = 10)).isoformat()


def big_value():
    '''
    This test is for multiple markers in acp talbe, 200km,400km,600km,700km and 1000km brevet
    '''
    date = arrow.Arrow(2020,11,9)

    assert acp_times.open_time(700,1000, arrow.get(date)) == (date.shift(hours = 22, minutes = 22)).isoformat()
    assert acp_times.close_time(700,1000, arrow.get(date)) == (date.shift(hours = 48, minutes = 45)).isoformat()


def equal_value():
    '''
    This test is for testing what happen if control distance is same with brevet distance with no intermediate points.
    This would become boundary case.
    '''

    date = arrow.Arrow(2020,11,9)

    assert acp_times.open_time(400,400, arrow.get(date)) == (date.shift(hours = 12, minutes = 8)).isoformat()
    assert acp_times.close_time(400, 400, arrow.get(date)) == (date.shift(hours = 27, minutes = 0)).isoformat()


def wierd_value():
    '''
    This test is for value that is returned by our course number.
    '''
    date = arrow.Arrow(2020, 11, 9)

    assert acp_times.open_time(322, 400, arrow.get(date)) == (date.shift(hours=9, minutes=42)).isoformat()
    assert acp_times.close_time(322, 400, arrow.get(date)) == (date.shift(hours=21, minutes=28)).isoformat()

def different_value():
    '''
        This test is for checking non-equal condition.
    '''
    date = arrow.Arrow(2020, 11, 9)

    assert acp_times.open_time(200, 400, arrow.get(date)) != (date.shift(hours=3, minutes=53)).isoformat()
    assert acp_times.close_time(200, 400, arrow.get(date)) != (date.shift(hours=11, minutes=20)).isoformat()
